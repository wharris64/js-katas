const output = document.getElementById('scripts')

//kata 1
output.innerHTML += "<p>Kata 1</p>"
for (i = 1; i <= 20; i++) {
    output.innerHTML += '<p>Kata 1: ' + i + '</p>'
}

//Kata2

output.innerHTML += "<p>Kata 2</p>"
for(i = 1; i <= 20; i++){
    if (i % 2 == 0){
        output.innerHTML += '<p>Kata 2: ' + i + '</p>'
    }
} 

//Kata3
output.innerHTML += "<p>Kata 3</p>"
for(i = 1; i <= 20; i++){
    if (i % 2 == 1){
        output.innerHTML += '<p>Kata 3: ' + i + '</p>'
    }
} 

//Kata4
output.innerHTML += "<p>Kata 4</p>"
for(i = 5; i <= 100; i++) {
    if (i % 5 == 0){
        output.innerHTML += '<p>Kata 4: ' + i + ' </p>'
    }
}


//Kata5
output.innerHTML += "<p>Kata 5</p>"
for (i = 1; i <= 10; i++){
    let result = i*i
    output.innerHTML += '<p>Kata 5: ' + result + ' </p>'
}

// Kata 6
for (i = 20; i >= 1; i --){
    output.innerHTML += '<p>Kata 6:' + i + '</p>'
}

//Kata 7
for (i=20; i >= 1; i --){
    if ( i % 2 == 0) {
        output.innerHTML += '<p>Kata 7:' + i + '</p>'
    }

}

//Kata 8
for (i=20; i >= 1; i --){
    if ( i % 2 == 1) {
        output.innerHTML += '<p>Kata 8:' + i + '</p>'
    }

}

//Kata 9
for (i=100; i >= 1; i --){
    if (i % 5 == 0){
        output.innerHTML += '<p>Kata 9: ' + i + ' </p>'
    }
}

//Kata 10
for (i=10; i >= 1; i --){
    let result = i*i
    output.innerHTML += '<p>Kata 10: ' + result + ' </p>'
}